//
//  main.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
