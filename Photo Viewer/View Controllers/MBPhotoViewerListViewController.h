//
//  ViewController.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import <Cocoa/Cocoa.h>
#import "MBRootViewController.h"
#import "MBImageFileDropValidator.h"
#import "MBImagePreviewCreator.h"
#import "MBImageBlurringComponent.h"
#import "MBImagePreviewPresenter.h"

@interface MBPhotoViewerListViewController : MBRootViewController<NSCollectionViewDelegate, NSCollectionViewDataSource>
@property (weak) IBOutlet NSCollectionView *collectionView;
@property (weak) IBOutlet NSTextField *initialTitleLabel;
@property (weak) IBOutlet NSScrollView *scrollView;

@property (nonatomic, strong)  MBImagePreviewPresenter *imagePresenter;
@property (nonatomic, strong)  MBImageFileDropValidator *dropValidator;
@property (nonatomic, strong)  MBImagePreviewCreator *imagePreviewCreator;
@property (nonatomic, strong)  MBImageBlurringComponent *imageBlurringComponent;

@end

