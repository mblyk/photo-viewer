//
//  MBPhotoViewerViewController.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/15/17.
//

#import <Cocoa/Cocoa.h>
#import "MBRootViewController.h"
#import "MBImageBlurringComponent.h"

@interface MBPhotoViewerViewController : MBRootViewController
@property (weak) IBOutlet NSImageView *imageView;
@property (weak) IBOutlet NSButton *previousButton;
@property (weak) IBOutlet NSButton *nextButton;

@property (nonatomic, strong)  NSArray<NSURL *> *photoURLs;
@property (nonatomic, assign)  NSUInteger currentIndex;

@property (nonatomic, strong)  MBImageBlurringComponent *imageBlurringComponent;

@end
