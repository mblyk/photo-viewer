//
//  MBPhotoViewerViewController.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/15/17.
//

#import "MBPhotoViewerViewController.h"
#import "MBImageBlurringComponent.h"
#import "NSImage+MBExtension.h"

@implementation MBPhotoViewerViewController

- (void)setupActions {
    self.nextButton.action = @selector(onNext);
    self.nextButton.target = self;
    
    self.previousButton.action = @selector(onPrevious);
    self.previousButton.target = self;
}

- (void)setupButtonsImages {
    self.previousButton.image = [NSImage arrowUpImage];
    self.nextButton.image = [NSImage arrowDownImage];
}

- (void)setupImageView {
    self.imageView.wantsLayer = YES;
    [self.imageView.layer setContentsGravity:kCAGravityResizeAspectFill];
    
    
    [self.imageView setContentCompressionResistancePriority:400
                                             forOrientation:NSLayoutConstraintOrientationHorizontal];
    [self.imageView setContentCompressionResistancePriority:400
                                             forOrientation:NSLayoutConstraintOrientationVertical];
}

- (void)setupViewController {
    [self setupBackgroundColor];
    [self setupActions];
    [self setupButtonsImages];
    [self setupImageView];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    
    [self setupLocalMonitorForKeyDownEvent];
    
    [self showCurrentImage];
}

- (void)onPrevious {
    if (self.currentIndex > 0) {
        self.currentIndex--;
        [self showCurrentImage];
    }
}

- (void)showCurrentImage {
    self.imageView.image = [[NSImage alloc] initByReferencingURL:self.photoURLs[self.currentIndex]];
}

- (void)keyDown:(NSEvent *)event {
    if ([event keyCode] == 126) {
        [self onPrevious];
    } else if ([event keyCode] == 125) {
        [self onNext];
    }
}

- (void)onNext {
    if (self.currentIndex < (self.photoURLs.count -1)) {
        self.currentIndex++;
        [self showCurrentImage];
    }
}

- (IBAction)onBlur:(id)sender {
    NSImage *blurredImage = [self.imageBlurringComponent blurredImageWithImage:self.imageView.image];
    
    self.imageView.image = blurredImage;
}

@end
