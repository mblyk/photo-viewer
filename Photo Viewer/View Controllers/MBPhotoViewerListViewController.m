//
//  ViewController.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import "MBPhotoViewerListViewController.h"
#import "MBPhotoCollectionViewItem.h"
#import "NSViewController+MBExtension.h"
#import "MBPhotoViewerViewController.h"
#import "NSColor+MBExtensions.h"

static CGFloat const MBDefaultCellSize = 200.f;
static CGFloat const MBDefaultSpacing = 5.f;

@interface MBPhotoViewerListViewController()<NSCollectionViewDelegateFlowLayout, MBPhotoCollectionViewItemDoubleClickDelegate>
@property (nonatomic, strong)  NSMutableArray<NSURL *> *photosURLs;
@property (nonatomic, strong)  NSCollectionViewFlowLayout *flowLayout;

@end

@implementation MBPhotoViewerListViewController

#pragma mark -
#pragma mark Public Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.photosURLs = [NSMutableArray new];
    
    [self setupLocalMonitorForKeyDownEvent];
    
    [self setupCollectionView];
    [self setupBackgroundColor];
}

- (NSSize)collectionView:(NSCollectionView *)collectionView
                  layout:(NSCollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return NSSizeFromCGSize(CGSizeMake(MBDefaultCellSize, MBDefaultCellSize));
}

- (NSInteger)collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosURLs.count;
}

- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath {
    MBPhotoCollectionViewItem *item = [collectionView makeItemWithIdentifier:@"Cell" forIndexPath:indexPath];
    item.delegate = self;
    
    NSURL *URL = self.photosURLs[indexPath.item];
    [self.imagePresenter presentImageAtURL:URL onView:item];
    
    
    return item;
}

- (NSDragOperation)collectionView:(NSCollectionView *)collectionView validateDrop:(id <NSDraggingInfo>)draggingInfo proposedIndexPath:(NSIndexPath **)proposedDropIndexPath dropOperation:(NSCollectionViewDropOperation *)proposedDropOperation {
    *proposedDropOperation = NSCollectionViewDropBefore;
    return [self.dropValidator validateDrop:draggingInfo];
}

- (BOOL)collectionView:(NSCollectionView *)collectionView acceptDrop:(id<NSDraggingInfo>)draggingInfo indexPath:(NSIndexPath *)index dropOperation:(NSCollectionViewDropOperation)dropOperation {
    
    NSPasteboard *pasteboard = [draggingInfo draggingPasteboard];
    NSArray *URLs = [self.dropValidator filteredURLsFromPasteboard:pasteboard];
    
    __block NSInteger current = index.item;
    
    [self.imagePreviewCreator createImagePreviewsFromImages:URLs completion:^(NSInteger processedBatchSize) {
        NSMutableSet *set = [NSMutableSet new];
        
        for (int i = 0; i < processedBatchSize; i++) {
            [set addObject:[NSIndexPath indexPathForItem:current inSection:0]];
            current++;
        }
        
        [self.collectionView reloadItemsAtIndexPaths:set];
    }];

    [self insertPhotos:URLs indexPath:index];
    [self.collectionView reloadData];
    
    [self hideInitialTitle];
    
    return YES;
}


- (void)doubleClickOn:(MBPhotoCollectionViewItem *)item {
    NSStoryboard *storyboard = [NSStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    
    MBPhotoViewerViewController *vc = [storyboard instantiateControllerWithIdentifier:[MBPhotoViewerViewController className]];
    
    vc.photoURLs = self.photosURLs;
    vc.currentIndex = [self.collectionView indexPathForItem:item].item;
    vc.imageBlurringComponent = self.imageBlurringComponent;
    
    [self addFullScreenChildViewController:vc];
}

- (void)keyDown:(NSEvent *)event {
    if ([event keyCode] == 53) {
        [self removeChildViewController];
    }
}

#pragma mark -
#pragma mark Private Methods

- (void)insertPhotos:(NSArray<NSURL *> *)photos indexPath:(NSIndexPath *)indexPath {
    NSMutableSet *set = [NSMutableSet set];
    NSUInteger initial = indexPath.item;
    
    for (id el in photos) {
        [set addObject:[NSIndexPath indexPathForItem:initial inSection:0]];
        initial++;
    }
    
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(indexPath.item, initial - indexPath.item)];
    
    [self.photosURLs insertObjects:photos atIndexes:indexSet];
    [self.collectionView insertItemsAtIndexPaths:set];
}

- (void)setupCollectionView {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.flowLayout = [NSCollectionViewFlowLayout new];
    self.collectionView.collectionViewLayout = self.flowLayout;
    
    self.flowLayout.minimumInteritemSpacing = MBDefaultSpacing;
    self.flowLayout.minimumLineSpacing = MBDefaultSpacing;
    

    [self.collectionView registerClass:[MBPhotoCollectionViewItem class] forItemWithIdentifier:@"Cell"];
    
    [self.collectionView registerForDraggedTypes:@[(__bridge NSString *)kUTTypeImage, (__bridge NSString *)kUTTypeFileURL]];
}

- (void)setupBackgroundColor {
    [super setupBackgroundColor];
    self.collectionView.backgroundColors = @[[NSColor backgroundColor]];
}

- (void)hideInitialTitle {
    self.initialTitleLabel.alphaValue = 0.f;
}

@end
