//
//  MBRootViewController.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import <Cocoa/Cocoa.h>

@interface MBRootViewController : NSViewController

- (void)setupLocalMonitorForKeyDownEvent;

- (void)setupBackgroundColor;

@end
