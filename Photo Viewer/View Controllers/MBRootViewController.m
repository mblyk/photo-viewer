//
//  MBRootViewController.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import "MBRootViewController.h"
#import "NSColor+MBExtensions.h"

@interface MBRootViewController ()
@property (nonatomic, strong)  id monitor;

@end

@implementation MBRootViewController

- (void)dealloc {
    [NSEvent removeMonitor:self.monitor];
}

- (void)setupLocalMonitorForKeyDownEvent {
    __weak typeof(self) weakSelf = self;
    self.monitor = [NSEvent addLocalMonitorForEventsMatchingMask:NSEventMaskKeyDown
                                                         handler:^NSEvent *(NSEvent *event) {
                                                             [weakSelf keyDown:event];
                                                             return event;
                                                         }];
}

- (void)setupBackgroundColor {
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = [NSColor backgroundColor].CGColor;
}

@end
