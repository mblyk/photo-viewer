//
//  MBImagePresenter.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import <Foundation/Foundation.h>
#import "MBEmptyImagePreviewContainer.h"
#import "MBImageContainerView.h"
#import "MBImagePreviewCreator.h"

@interface MBImagePreviewPresenter : NSObject

- (instancetype)initWithImagePreviewPresenter:(MBEmptyImagePreviewContainer *)emptyImageContainer
                               previewCreator:(MBImagePreviewCreator *)previewCreator;

- (void)presentImageAtURL:(NSURL *)URL onView:(id<MBImageContainerView>)view;

@end
