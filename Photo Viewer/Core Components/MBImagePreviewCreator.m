//
//  MBImagePreviewCreator.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/14/17.
//

#import "MBImagePreviewCreator.h"
#import <ImageIO/ImageIO.h>

@import AppKit;

@interface MBImagePreviewCreator ()
@property (nonatomic, strong)  NSMutableDictionary *map;
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong)  NSString *temporaryDirectory;

@end

@implementation MBImagePreviewCreator

- (instancetype)init {
    self = [super init];
    if (self) {
        self.map = [NSMutableDictionary new];
        self.queue = dispatch_queue_create("image.resizing", NULL);
        self.temporaryDirectory = NSTemporaryDirectory();
    }
    
    return self;
}

- (BOOL)hasProcessedPreviewForURL:(NSURL *)URL {
    return self.map[URL] ? YES : NO;
}

- (NSImage *)imagePreviewForURL:(NSURL *)URL {
    NSString *filePath = self.map[URL];
    
    NSImage *image = nil;
    if (filePath) {
        image = [[NSImage alloc] initWithContentsOfFile:filePath];
    }
    
    return image;
}

- (void)createImagePreviewsFromImages:(NSArray<NSURL *> *)URLs completion:(void (^)(NSInteger batchSize))completion; {
    dispatch_async(self.queue, ^{
        NSUInteger counter = 0;
        NSUInteger batchSize = 5;
        
        for (NSURL *URL in URLs) {
            @autoreleasepool {
                CGImageRef image = [self resizedImageWithSize:400 atURL:URL];
                NSString *fileName = [NSUUID UUID].UUIDString;
                NSString *filePath = [self.temporaryDirectory stringByAppendingPathComponent:fileName];
                BOOL result = CGImageWriteToFile(image, filePath);
                
                CGImageRelease(image);
                
                if (result) {
                    self.map[URL] = filePath;
                }
                
                counter++;
                
                if ((counter % batchSize) == 0) {
                    if(completion) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(batchSize);
                        });
                    }
                }
            }
        }
        
        NSInteger remainder = counter % batchSize;
        if(completion && remainder != 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(remainder);
            });
        }
    });
}

BOOL CGImageWriteToFile(CGImageRef image, NSString *path) {
    CFURLRef url = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, kUTTypeJPEG, 1, NULL);
    if (!destination) {
        NSLog(@"Failed to create CGImageDestination for %@", path);
        return NO;
    }
    
    CGImageDestinationAddImage(destination, image, nil);
    
    if (!CGImageDestinationFinalize(destination)) {
        NSLog(@"Failed to write image to %@", path);
        CFRelease(destination);
        return NO;
    }
    
    CFRelease(destination);
    return YES;
}

- (CGSize)imageSizeAtImageSource:(CGImageSourceRef)imageSource {
    CGFloat width = 0.0f, height = 0.0f;
    CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
    
    if (imageProperties != NULL) {
        CFNumberRef widthNum  = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
        if (widthNum != NULL) {
            CFNumberGetValue(widthNum, kCFNumberCGFloatType, &width);
        }
        
        CFNumberRef heightNum = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
        if (heightNum != NULL) {
            CFNumberGetValue(heightNum, kCFNumberCGFloatType, &height);
        }
        
        // Check orientation and flip size if required
        CFNumberRef orientationNum = CFDictionaryGetValue(imageProperties, kCGImagePropertyOrientation);
        if (orientationNum != NULL) {
            int orientation;
            CFNumberGetValue(orientationNum, kCFNumberIntType, &orientation);
            if (orientation > 4) {
                CGFloat temp = width;
                width = height;
                height = temp;
            }
        }
        
        CFRelease(imageProperties);
    }
    
    return CGSizeMake(width, height);
}

- (CGImageRef)resizedImageWithSize:(CGFloat)size atURL:(NSURL *)URL {
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)URL, NULL);
    
    CGFloat width = 0.0f, height = 0.0f;
    
    CGSize imageSize = [self imageSizeAtImageSource:imageSource];
    width = imageSize.width;
    height = imageSize.height;
    
    CGFloat ratio = floor(fmax(width, height) / size);
    
    int maxSize =  (int)(fmax(width, height) / ratio);
    
    CFDictionaryRef   myOptions = NULL;
    CFStringRef       myKeys[4];
    CFTypeRef         myValues[4];
    CFNumberRef       thumbnailSize;
    
    if (imageSource == NULL){
        fprintf(stderr, "Image source is NULL.");
        return  NULL;
    }
    
    CGImageRef        thumbnailImage = NULL;
    
    thumbnailSize = CFNumberCreate(NULL, kCFNumberIntType, &maxSize);
    
    myKeys[0] = kCGImageSourceCreateThumbnailWithTransform;
    myValues[0] = (CFTypeRef)kCFBooleanTrue;
    myKeys[1] = kCGImageSourceCreateThumbnailFromImageIfAbsent;
    myValues[1] = (CFTypeRef)kCFBooleanTrue;
    myKeys[2] = kCGImageSourceThumbnailMaxPixelSize;
    myValues[2] = (CFTypeRef)thumbnailSize;
    myKeys[3] = kCGImageSourceCreateThumbnailFromImageAlways;
    myValues[3] = (CFTypeRef)kCFBooleanTrue;
    
    myOptions = CFDictionaryCreate(NULL, (const void **) myKeys,
                                   (const void **) myValues, 4,
                                   &kCFTypeDictionaryKeyCallBacks,
                                   & kCFTypeDictionaryValueCallBacks);
    
    thumbnailImage = CGImageSourceCreateThumbnailAtIndex(imageSource,
                                                         0,
                                                         myOptions);
    
    CFRelease(thumbnailSize);
    CFRelease(myOptions);
    CFRelease(imageSource);
    
    if (thumbnailImage == NULL){
        fprintf(stderr, "Thumbnail image not created from image source.");
        return NULL;
    }
    
    return thumbnailImage;
}

@end
