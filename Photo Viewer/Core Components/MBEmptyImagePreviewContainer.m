//
//  MBPreviewImagePresenter.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import "MBEmptyImagePreviewContainer.h"

@implementation MBEmptyImagePreviewContainer

- (instancetype)initWithPreviewImageName:(NSString *)previewImageName {
    self = [super init];
    if (self) {
        _previewImage = [NSImage imageNamed:previewImageName];
    }
    
    return self;
}

@end
