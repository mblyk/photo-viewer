//
//  MBImageBluringComponent.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import <Foundation/Foundation.h>

@interface MBImageBlurringComponent : NSObject

- (NSImage *)blurredImageWithImage:(NSImage *)sourceImage;

@end
