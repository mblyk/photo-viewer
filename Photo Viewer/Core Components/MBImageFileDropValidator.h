//
//  MBImageFileDropValidator.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/14/17.
//

#import <Foundation/Foundation.h>

@import AppKit;

@interface MBImageFileDropValidator : NSObject

- (NSDragOperation)validateDrop:(id <NSDraggingInfo>)draggingInfo;

- (BOOL)isValidItemAtURL:(NSURL *)fileURL;

- (NSArray<NSURL *> *)filteredURLsFromPasteboard:(NSPasteboard *)pasteboard;

@end
