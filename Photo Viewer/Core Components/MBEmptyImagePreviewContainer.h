//
//  MBPreviewImagePresenter.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import <Foundation/Foundation.h>

@import AppKit;

@interface MBEmptyImagePreviewContainer : NSObject
@property (nonatomic, readonly)  NSImage *previewImage;

- (instancetype)initWithPreviewImageName:(NSString *)previewImageName;

@end
