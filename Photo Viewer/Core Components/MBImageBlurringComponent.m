//
//  MBImageBluringComponent.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import "MBImageBlurringComponent.h"

@import CoreImage;
@import AppKit;

@implementation MBImageBlurringComponent

- (NSImage *)blurredImageWithImage:(NSImage *)sourceImage {
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithData:[sourceImage TIFFRepresentation]];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];

    
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    NSImage *returnValue = [[NSImage alloc] initWithCGImage:cgImage size:NSZeroSize];
    
    if (cgImage) {
        CGImageRelease(cgImage);
    }
    
    return returnValue;
}

@end
