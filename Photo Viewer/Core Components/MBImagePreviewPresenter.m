//
//  MBImagePresenter.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import "MBImagePreviewPresenter.h"

@interface MBImagePreviewPresenter ()
@property (nonatomic, strong)  MBEmptyImagePreviewContainer *emptyImageContainer;
@property (nonatomic, strong)  MBImagePreviewCreator *previewCreator;

@end

@implementation MBImagePreviewPresenter


- (instancetype)initWithImagePreviewPresenter:(MBEmptyImagePreviewContainer *)emptyImageContainer
                               previewCreator:(MBImagePreviewCreator *)previewCreator {
    self = [super init];
    if (self) {
        self.emptyImageContainer = emptyImageContainer;
        self.previewCreator = previewCreator;
    }
    
    return self;
}

- (void)presentImageAtURL:(NSURL *)URL onView:(id<MBImageContainerView>)view {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
        NSImage *image;
        if ([self.previewCreator hasProcessedPreviewForURL:URL]) {
            image = [self.previewCreator imagePreviewForURL:URL];
        } else {
            image = [self.emptyImageContainer previewImage];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [view setImage:image];
        });
    });
}

@end
