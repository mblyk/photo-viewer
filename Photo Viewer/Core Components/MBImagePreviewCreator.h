//
//  MBImagePreviewCreator.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/14/17.
//

#import <Foundation/Foundation.h>

@import AppKit;

@interface MBImagePreviewCreator : NSObject

- (void)createImagePreviewsFromImages:(NSArray<NSURL *> *)URLs completion:(void (^)(NSInteger batchSize))completion;

- (NSImage *)imagePreviewForURL:(NSURL *)URL;

- (BOOL)hasProcessedPreviewForURL:(NSURL *)URL;

@end
