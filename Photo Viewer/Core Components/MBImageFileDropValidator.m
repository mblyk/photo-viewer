//
//  MBImageFileDropValidator.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/14/17.
//

#import "MBImageFileDropValidator.h"

@implementation MBImageFileDropValidator

- (NSDragOperation)validateDrop:(id<NSDraggingInfo>)draggingInfo {
    NSPasteboard *pasteboard = [draggingInfo draggingPasteboard];
    NSDictionary *options = [NSDictionary dictionaryWithObject:@YES
                                                        forKey:NSPasteboardURLReadingFileURLsOnlyKey];
    
    NSArray<NSURL *> *results = [pasteboard readObjectsForClasses:[NSArray arrayWithObject:[NSURL class]] options:options];
    
    for (NSURL *el in results) {
        if ([self isImageSource:el]) {
            return NSDragOperationMove;
        }
    }
    
    return NSDragOperationNone;
}

-(BOOL)isImageSource:(NSURL*)URL {
    CGImageSourceRef source = CGImageSourceCreateWithURL((__bridge CFURLRef)URL, NULL);
    if (!source) {
        return NO;
    }
    
    NSString *UTI = (__bridge NSString*)CGImageSourceGetType(source);
    CFRelease(source);
    
    CFArrayRef mySourceTypes = CGImageSourceCopyTypeIdentifiers();
    NSArray *array = (__bridge NSArray*)mySourceTypes;
    CFRelease(mySourceTypes);
    
    return [array containsObject:UTI];
}


- (BOOL)isValidItemAtURL:(NSURL *)fileURL {
    return [self isImageSource:fileURL];
}

- (NSArray<NSURL *> *)filteredURLsFromPasteboard:(NSPasteboard *)pasteboard {
    NSMutableArray<NSURL *> *URLs = [NSMutableArray new];
    
    NSDictionary *options = [NSDictionary dictionaryWithObject:@YES
                                                        forKey:NSPasteboardURLReadingFileURLsOnlyKey];
    NSArray<NSURL *> *results = [pasteboard readObjectsForClasses:[NSArray arrayWithObject:[NSURL class]] options:options];
    
    for (NSURL *URL in results) {
        if ([self isValidItemAtURL:URL]) {
            [URLs addObject:URL];
        }
    }
    
    return URLs;
}

@end
