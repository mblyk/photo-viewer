//
//  AppDelegate.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

