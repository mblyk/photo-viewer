//
//  AppDelegate.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import "AppDelegate.h"
#import "MBPhotoViewerListViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    MBPhotoViewerListViewController *rootVC = (MBPhotoViewerListViewController *)[[[NSApplication sharedApplication] keyWindow] contentViewController];
    
    MBEmptyImagePreviewContainer *emptyImageContainer = [[MBEmptyImagePreviewContainer alloc] initWithPreviewImageName:@"preview.png"];
    
    MBImagePreviewCreator *imagePreviewCreator = [MBImagePreviewCreator new];
    MBImagePreviewPresenter *imagePresenter = [[MBImagePreviewPresenter alloc] initWithImagePreviewPresenter:emptyImageContainer
                                                                                previewCreator:imagePreviewCreator];
    
    rootVC.dropValidator = [MBImageFileDropValidator new];
    rootVC.imageBlurringComponent = [MBImageBlurringComponent new];
    rootVC.imagePreviewCreator = imagePreviewCreator;
    rootVC.imagePresenter = imagePresenter;
}

@end
