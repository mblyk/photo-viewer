//
//  NSColor+MBExtensions.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (MBExtensions)

+ (NSColor *)backgroundColor;

@end
