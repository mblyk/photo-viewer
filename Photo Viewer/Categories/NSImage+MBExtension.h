//
//  NSImage+MBExtension.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (MBExtension)

+ (NSImage *)arrowUpImage;
+ (NSImage *)arrowDownImage;

@end
