//
//  NSColor+MBExtensions.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import "NSColor+MBExtensions.h"

@implementation NSColor (MBExtensions)

+ (NSColor *)backgroundColor {
    return [NSColor colorWithRed:222.f / 255
                           green:222.f / 255
                            blue:222.f / 255
                           alpha:1.f];
}

@end
