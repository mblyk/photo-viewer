//
//  NSViewController+MBExtension.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import <Cocoa/Cocoa.h>

@interface NSViewController (MBExtension)

//naming?
- (void)addFullScreenChildViewController:(NSViewController *)childViewController;
- (void)removeChildViewController;

@end
