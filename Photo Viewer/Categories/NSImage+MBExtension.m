//
//  NSImage+MBExtension.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/17/17.
//

#import "NSImage+MBExtension.h"

@implementation NSImage (MBExtension)

+ (NSImage *)arrowUpImage {
    return [NSImage imageNamed:@"arrow-up.png"];
}

+ (NSImage *)arrowDownImage {
    return [NSImage imageNamed:@"arrow-down.png"];
}

@end
