//
//  NSViewController+MBExtension.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import "NSViewController+MBExtension.h"

@implementation NSViewController (MBExtension)

- (void)removeChildViewController {
    [self.childViewControllers.firstObject.view removeFromSuperview];
    [self removeChildViewControllerAtIndex:0];
}

- (void)addFullScreenChildViewController:(NSViewController *)childViewController {
    childViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addChildViewController:childViewController];
    [self.view addSubview:childViewController.view];
    
    [self setupConstraintsWithChildVC:childViewController];
}

- (void)setupConstraintsWithChildVC:(NSViewController *)child {
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:child.view
                                                            attribute:NSLayoutAttributeCenterX
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:self.view
                                                            attribute:NSLayoutAttributeCenterX
                                                           multiplier:1.f
                                                             constant:0.f];
    
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:child.view
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.f
                                                              constant:0.f];
    
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:child.view
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.view
                                                           attribute:NSLayoutAttributeWidth
                                                          multiplier:1.f
                                                            constant:0.f];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:child.view
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.f
                                                               constant:0.f];
    
    [self.view addConstraints:@[left, right, bottom, top]];
}

@end
