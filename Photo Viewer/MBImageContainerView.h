//
//  MBImageContainerView.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/18/17.
//

#import <Foundation/Foundation.h>

@class NSImage;

@protocol MBImageContainerView <NSObject>

- (void)setImage:(NSImage *)image;

@end
