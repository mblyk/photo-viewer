//
//  MBPhotoCollectionViewItem.h
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import <Cocoa/Cocoa.h>
#import "MBImageContainerView.h"

@class MBPhotoCollectionViewItem;

@protocol MBPhotoCollectionViewItemDoubleClickDelegate<NSObject>

- (void)doubleClickOn:(MBPhotoCollectionViewItem *)item;

@end

@interface MBPhotoCollectionViewItem : NSCollectionViewItem<MBImageContainerView>
@property (nonatomic, weak)  id<MBPhotoCollectionViewItemDoubleClickDelegate> delegate;

@end
