//
//  MBPhotoCollectionViewItem.m
//  Photo Viewer
//
//  Created by Mykola Blyk on 12/13/17.
//

#import "MBPhotoCollectionViewItem.h"

@implementation MBPhotoCollectionViewItem

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageView.wantsLayer = YES;
    [self.imageView.layer setContentsGravity:kCAGravityResizeAspectFill];
    
    self.imageView.imageScaling = NSImageScaleProportionallyUpOrDown;
}

- (void)setImage:(NSImage *)image {
    self.imageView.image = image;
}

- (void)prepareForReuse {
    self.imageView.image = nil;
}

- (void)mouseDown:(NSEvent *)event {
    [super mouseDown:event];

    if([event clickCount] > 1) {
        if(self.delegate && [self.delegate respondsToSelector:@selector(doubleClickOn:)]) {
            [self.delegate performSelector:@selector(doubleClickOn:) withObject:self];
        }
    }
}
@end
